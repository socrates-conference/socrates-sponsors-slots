# socrates-sponsors-slots

This application administers sponsor slots for the SoCraTes Website.
It is written in TypeScript, built using yarn and webpack, and deployed via AWS SAM.

- Test: `yarn test`
- Build: `yarn build`
- Deploy: `yarn deploy`
- Clean SAM output folder: `yarn clean`

A CI build is triggered for every change you commit to Gitlab. Rollout to AWS can be triggered manually using the Gitlab console.