import {DynamoDB} from '@aws-sdk/client-dynamodb'
import {
  APIGatewayProxyEvent,
  APIGatewayProxyResult
} from 'aws-lambda'
import {
  failure,
  success,
} from '../common/util'

const tableName: string = process.env.SLOTS_TABLE

export const deleteSlot = (dynamoDB: DynamoDB) => async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
  const conference = event?.pathParameters?.conference
  const id = event?.pathParameters?.id
  if (conference !== undefined && id !== undefined) {
    try {
      await dynamoDB.deleteItem({TableName: tableName, Key: {id: {S: id}, conference: {S:conference}}})
      return success()
    } catch (e) {
      return e.__type.endsWith('ResourceNotFoundException')
             ? failure(404, e.message)
             : failure(500, e.message)
    }
  } else {
    return failure(400, 'Invalid parameters.')
  }
}