import {deleteSlot} from './deleteSlot'
import {DynamoDB} from '@aws-sdk/client-dynamodb'

export const handler = deleteSlot(new DynamoDB({region: 'eu-central-1'}))
