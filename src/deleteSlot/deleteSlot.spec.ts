process.env.SLOTS_TABLE = 'slots'
import {deleteSlot} from './deleteSlot'
import {
  DynamoKey,
  DynamoSlot,
  FakeDynamoDB
} from '../mock/dynamoDB'
import {DynamoDB} from '@aws-sdk/client-dynamodb'
import {
  APIGatewayProxyEvent,
  APIGatewayProxyResult
} from 'aws-lambda'


describe('DELETE /slots:', function () {
  const inputEvent: APIGatewayProxyEvent = {
    pathParameters: {
      id: '1',
      conference: '1'
    }
  } as unknown as APIGatewayProxyEvent
  let fakeDynamoDB: FakeDynamoDB<DynamoSlot>
  beforeEach(() => {
    fakeDynamoDB = new FakeDynamoDB(['id', 'conference'])
  })

  describe('when an item exists', () => {
    const SPON_KEY: DynamoKey & { conference: { S: string } } = {id: {S: '1'}, conference: {S: '1'}}
    const SLOT: DynamoSlot = {
      conference: {S: '1'},
      id: {S: '1'},
      name: {S: 'Some Co.'},
      logo: {S: 'http://google.de'},
      slot: {S: 'Gold'},
      url: {S:'http://some.where'},
      donation: {S: '500€'},
      paymentReceived: {BOOL: true}
    }
    let result: APIGatewayProxyResult
    beforeEach(async () => {
      fakeDynamoDB._items.set(JSON.stringify(SPON_KEY), SLOT)
      result = await deleteSlot(fakeDynamoDB as unknown as DynamoDB)(inputEvent)
    })
    it('should delete from table "slots"', function () {
      expect(fakeDynamoDB.table).toEqual('slots')
    })
    it('should return 200', async () => {
      expect(result.statusCode).toEqual(200)
    })
  })

  describe('when an item does not exist', () => {
    let result: APIGatewayProxyResult
    beforeEach(async () => {
      result = await deleteSlot(fakeDynamoDB as unknown as DynamoDB)(inputEvent)
    })


    it('should return 404', () => {
      expect(result.statusCode).toEqual(404)
    })
  })
})