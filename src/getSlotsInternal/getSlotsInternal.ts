import {
  DynamoDB,
  GetItemCommandOutput,
  ScanCommandOutput
} from '@aws-sdk/client-dynamodb'
import {
  APIGatewayProxyEvent,
  APIGatewayProxyResult
} from 'aws-lambda'
import {SponsorSlot} from '../common/types'
import {
  failure,
  success
} from '../common/util'

const tableName: string = process.env.SLOTS_TABLE

const fromItem = (item): SponsorSlot => {
  const slot = {}
  Object.entries(item).forEach(([key, value]) => slot[key] = value[Object.keys(value)[0]])
  return slot as SponsorSlot
}

export const getSlotsInternal = (dynamoDB: DynamoDB) => async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
  const conference: string | undefined = event?.pathParameters?.conference
  if (conference !== undefined) {
    const id: string | undefined = event?.pathParameters?.id
    if (id !== undefined) {
      const result: GetItemCommandOutput = await dynamoDB.getItem({
        TableName: tableName,
        Key: { 'conference': {'S': conference}, 'id': {'S': id}}
      })
      if (result.Item !== undefined) {
        return success(fromItem(result.Item))
      } else {
        return failure(404, `A sponsor slot for conference ${conference} with id:${id} does not exist`)
      }
    } else {
      const result: ScanCommandOutput = await dynamoDB.scan({TableName: tableName, FilterExpression: 'conference = :val', ExpressionAttributeValues:{':val': {'S':conference}}})
      return success(result.Items.map(fromItem))
    }
  } else return failure(400, 'Missing conference id.')
}