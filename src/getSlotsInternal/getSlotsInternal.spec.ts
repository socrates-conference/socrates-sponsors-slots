process.env.SLOTS_TABLE = 'slots'
import {APIGatewayProxyEvent} from 'aws-lambda'
import {getSlotsInternal} from './getSlotsInternal'
import {
  DynamoSlot,
  FakeDynamoDB
} from '../mock/dynamoDB'
import {DynamoDB} from '@aws-sdk/client-dynamodb'

describe('GET /slots', () => {
  let fakeDynamoDB: FakeDynamoDB<DynamoSlot>
  beforeEach(() => {
    fakeDynamoDB = new FakeDynamoDB(['id', 'conference'])
  })

  describe('without id path parameter', () => {
    it('should return empty list', async () => {
      const result = await getSlotsInternal(fakeDynamoDB as unknown as DynamoDB)({pathParameters: {conference: '1'}} as unknown as APIGatewayProxyEvent)
      expect(result.statusCode).toEqual(200)
      expect(fakeDynamoDB.table).toEqual('slots')
      expect(JSON.parse(result.body)).toEqual([])
    })

    describe('when a slot exists', () => {
      const SLOT: DynamoSlot = {
        conference: {S: '1'},
        id: {S: '1'},
        name: {S: 'Some Co.'},
        slot: {S: 'Gold'},
        donation: {S: '500€'},
        url: {S:'http://some.where'},
        paymentReceived: {BOOL: true},
        logo: {S: 'http://google.de'}
      }
      beforeEach(() => {
        fakeDynamoDB._items.set(JSON.stringify({id: {S: '1'}, conference: {S: '1'}}), SLOT)
      })

      it('should return list of one', async () => {
        const result = await getSlotsInternal(fakeDynamoDB as unknown as DynamoDB)({pathParameters:{conference:'1'}} as unknown as APIGatewayProxyEvent)
        expect(result.statusCode).toEqual(200)
        expect(fakeDynamoDB.table).toEqual('slots')
        expect(JSON.parse(result.body)).toEqual([{
          conference: '1',
          id: '1',
          name: 'Some Co.',
          slot: 'Gold',
          donation: '500€',
          url: 'http://some.where',
          paymentReceived: true,
          logo: 'http://google.de'
        }])
      })
    })
  })
})