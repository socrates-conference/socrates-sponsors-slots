import {DynamoDB} from '@aws-sdk/client-dynamodb'
import {getSlotsInternal} from './getSlotsInternal'

export const handler = getSlotsInternal(new DynamoDB({region: 'eu-central-1'}))