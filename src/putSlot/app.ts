import {putSlot} from './putSlot'
import {DynamoDB} from '@aws-sdk/client-dynamodb'

export const handler = putSlot(new DynamoDB({region: 'eu-central-1'}))