process.env.SLOTS_TABLE = 'slots'
import {APIGatewayProxyResult} from 'aws-lambda'
import {DynamoDB} from '@aws-sdk/client-dynamodb'
import {putSlot} from './putSlot'
import {
  DynamoSlot,
  FakeDynamoDB
} from '../mock/dynamoDB'

const SLOT = {
  conference: '1',
  name: 'Some Co.',
  slot: 'Gold',
  donation: '500€',
  paymentReceived: true,
  logo: 'http://google.de'
}

describe('PUT /slots', () => {
  let result: APIGatewayProxyResult
  const mock: FakeDynamoDB<DynamoSlot> = new FakeDynamoDB()

  beforeAll(async () => {
    const event: any = {body: JSON.stringify(SLOT)}
    result = await putSlot(mock as unknown as DynamoDB)(event)
  })

  it('should return status 200', async () => {
    expect(result.statusCode).toEqual(200)
  })

  it('should store a valid slot', async () => {
    expect(mock.table).toEqual('slots')
    expect((await mock.scan({TableName: process.env.SLOTS_TABLE})).Items[0].name).toEqual({"S":"Some Co."})
  })
})