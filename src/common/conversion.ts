import {v4 as uuid} from 'uuid'
import {UUID} from './types'

export type KeyValueType = { key?: UUID, value?: any }

const valueType = (value: any) => {
  if (typeof value === 'string') {
    return 'S'
  }
  if (typeof value === 'number') {
    return 'N'
  }
  if (typeof value === 'boolean') {
    return 'BOOL'
  }
}
export const toDynamoItemInput = (body: string, tableName: string) => {
  const payload: KeyValueType = JSON.parse(body)
  const {key, value} = payload
  const item = key !== undefined ? value : payload
  const Key = key !== undefined ? key : uuid()
  const Item = {id: {S: Key}}
  Object.entries(item).forEach(([key, value]) => Item[key] = {[valueType(value)]: value})
  return {TableName: tableName, Item}
}