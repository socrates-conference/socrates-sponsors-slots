export type URL = string
export type UUID = string
export type Sponsor = {
  id: UUID,
  name: string,
  url: URL,
  contactName: string;
  contactEmail: string,
  contactPhone: string,
  logo: string
}

export type Slot = 'Gold' | 'Silver' | 'Bronze'

export type SponsorSlotExternal = {
  name: string,
  url: URL,
  logo: string
}
export type SponsorSlot = {
  conference: UUID,
  id: UUID,
  name: string,
  url: URL,
  logo: string,
  slot: Slot;
  donation: string,
  paymentReceived: boolean
}