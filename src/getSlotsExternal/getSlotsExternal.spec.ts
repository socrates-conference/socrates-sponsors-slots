process.env.SLOTS_TABLE = 'slots'
import {APIGatewayProxyEvent} from 'aws-lambda'
import {getSlotsExternal} from './getSlotsExternal'
import {
  DynamoSlot,
  FakeDynamoDB
} from '../mock/dynamoDB'
import {DynamoDB} from '@aws-sdk/client-dynamodb'

describe('GET /slots/public', () => {
  let fakeDynamoDB: FakeDynamoDB<DynamoSlot>
  beforeEach(() => {
    fakeDynamoDB = new FakeDynamoDB(['id', 'conference'])
  })

  describe('without id path parameter', () => {
    it('should return empty list', async () => {
      const result = await getSlotsExternal(fakeDynamoDB as unknown as DynamoDB)({pathParameters: {conference: '1'}} as unknown as APIGatewayProxyEvent)
      expect(result.statusCode).toEqual(200)
      expect(fakeDynamoDB.table).toEqual('slots')
      expect(JSON.parse(result.body)).toEqual([])
    })

    describe('when a slot exists', () => {
      const SLOT: DynamoSlot = {
        conference: {S: '1'},
        id: {S: '1'},
        name: {S: 'Some Co.'},
        slot: {S: 'Gold'},
        donation: {S: '500€'},
        paymentReceived: {BOOL: true},
        url: {S:'http://some.where'},
        logo: {S: 'http://google.de'}
      }
      beforeEach(() => {
        fakeDynamoDB._items.set(JSON.stringify({id: {S: '1'}, conference: {S: '1'}}), SLOT)
      })

      it('should return list of one', async () => {
        const result = await getSlotsExternal(fakeDynamoDB as unknown as DynamoDB)({pathParameters: {conference: '1'}} as unknown as APIGatewayProxyEvent)
        expect(result.statusCode).toEqual(200)
        expect(fakeDynamoDB.table).toEqual('slots')
        expect(JSON.parse(result.body)).toEqual([{
          name: 'Some Co.',
          url: 'http://some.where',
          logo: 'http://google.de'
        }])
      })
    })
  })
})