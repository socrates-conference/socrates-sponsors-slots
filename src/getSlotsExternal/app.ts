import {DynamoDB} from '@aws-sdk/client-dynamodb'
import {getSlotsExternal} from './getSlotsExternal'

export const handler = getSlotsExternal(new DynamoDB({region: 'eu-central-1'}))