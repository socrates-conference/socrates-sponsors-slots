import {
  DynamoDB,
  ScanCommandOutput
} from '@aws-sdk/client-dynamodb'
import {
  APIGatewayProxyEvent,
  APIGatewayProxyResult
} from 'aws-lambda'
import {SponsorSlotExternal} from '../common/types'
import {
  failure,
  success
} from '../common/util'

const tableName: string = process.env.SLOTS_TABLE

const fromItem = (item): SponsorSlotExternal => {
  return {
    name: item.name?.S ?? '',
    url: item.url?.S ?? '',
    logo: item.logo?.S ?? ''
  }
}

export const getSlotsExternal = (dynamoDB: DynamoDB) => async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
  const conference: string | undefined = event?.pathParameters?.conference
  if (conference !== undefined) {
    const result: ScanCommandOutput = await dynamoDB.scan({
      TableName: tableName,
      FilterExpression: 'conference = :val',
      ExpressionAttributeValues: {':val': {'S': conference}}
    })
    return success(result.Items.map(fromItem))
  } else {
    return failure(400, 'Missing conference id.')
  }
}